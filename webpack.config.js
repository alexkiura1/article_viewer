const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

module.exports = {
    entry: './client/index.js',
    output: { path: path.resolve(__dirname, 'public/compiled'), filename: 'bundle.js' },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: /client/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },

    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true
            })
        ]
    }
};