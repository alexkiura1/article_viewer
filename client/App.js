import React from 'react';
import { Switch, Route } from "react-router-dom";
import ArticleHomeComponent from './components/ArticleHome';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';



const App = () => (
  <Container>
    <Navbar className="bg-light justify-content-between">
      <Navbar.Brand href="#home">Article Viewer</Navbar.Brand>
  </Navbar>
    <Switch>
        <Route exact path="/" component={ArticleHomeComponent} />
    </Switch>
  </Container>
);

export default App;