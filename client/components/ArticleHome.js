import React from 'react';
import Card from 'react-bootstrap/Card';
import axios from 'axios';
import moment from 'moment';
import Pagination from 'react-bootstrap/Pagination';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import Toast from 'react-bootstrap/Toast'
import Accordion from 'react-bootstrap/Accordion'


class ArticleComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            articleId:  props.article.id,
            articleTitle: props.article.title,
            articleBody: "",
            author: "",
            lastUpdated: props.article.updated_at,
            errors: false,
            errorMessage: ""
        }
        this.handleArticleDetail = this.handleArticleDetail.bind(this)
    }

    handleArticleDetail() {
        const url = "/v1/articles/" + this.state.articleId
        axios.get(url)
            .then((response) => {
                this.setState({
                    articleBody: response.data.translations[0].body,
                    author: response.data.last_publisher.name,
                    lastUpdated: response.data.updated_at
                })
            })
            .catch((exception) => {
                this.setState({
                    "errors": true,
                    "errorMessage": exception
                })
            })

    }

    componentDidMount() {
        this.handleArticleDetail()
    }

    render() {

        return (
            <Card>
                <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey={this.state.articleId}>
                        {this.state.articleTitle}
                    </Accordion.Toggle>
                </Card.Header>
                    <Accordion.Collapse eventKey={this.state.articleId}>
                <Card.Body>
                    < blockquote className = "blockquote mb-0" >
                        <p>{this.state.articleBody}</p>
                        <footer className="blockquote-footer">
                            Last updated <cite title="Last Updated">{moment(this.state.lastUpdated).fromNow() + " by " + this.state.author}</cite>
                        </footer>
                    </blockquote>

                </Card.Body>
                </Accordion.Collapse>
            </Card>
        )
    }
}

// const Page = ()

class ArticleHomeComponent extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            "articles": [],
            "nextPage": "",
            "count": 0,
            "currentPage": 0,
            "previousPage": "",
            "pageSize": 0,
            "errors": false,
            "errorMessage": "",
            "totalPages": 0,
            "searchTerm": "",
            "searchError": false
        }
        this.refreshArticleList = this.refreshArticleList.bind(this);
        this.handlePageLoad = this.handlePageLoad.bind(this)
        this.handleArticleSearch = this.handleArticleSearch.bind(this)
        this.handleSearchFieldUpdate = this.handleSearchFieldUpdate.bind(this)
        this.handleToastShow = this.handleToastShow.bind(this)
    }

    handleToastShow() {
        this.setState({searchError: !this.state.searchError})
    }
    refreshArticleList(url) {
        axios.get(url)
            .then((response) => {
                this.setState({
                    articles: response.data.articles,
                    nextPage: response.data.nextPage,
                    currentPage: response.data.currentPage,
                    pageSize: response.data.pageSize,
                    count: response.data.count,
                    previousPage: response.data.previousPage,
                    totalPages: response.data.totalPages
                })
            })
            .catch((exception) => {
                this.setState({"errors": true, "errorMessage": exception})
            })
    }
    handleArticleSearch() {
        const url = "/v1/search?query=" + this.state.searchTerm
        axios.get(url)
            .then((response) => {
                console.log(this.state.searchTerm, response)
                this.setState({
                    articles: response.data.results,
                    nextPage: response.data.nextPage,
                    currentPage: response.data.currentPage,
                    count: response.data.count,
                    previousPage: "",
                    totalPages: response.data.totalPages
                })
            })
            .catch((exception) => {
                this.setState({
                    "errors": true,
                    "errorMessage": exception,
                    "searchError": true
                })
            })
    }
    
    handleSearchFieldUpdate(event) {
        this.setState({searchTerm: event.target.value})
    }

    handlePageLoad(event) {
        event.preventDefault()
        console.log("clicked", event.target.href)
        this.refreshArticleList(event.target.href)
    }

    componentDidMount() {
        this.refreshArticleList("/v1/articles")
    }

    render() {
        return(
            <div>
                <Form inline>
                    <FormControl type="text" placeholder="Search by keyword" className=" mr-sm-2"
                        onChange={this.handleSearchFieldUpdate}/>
                    <Button onClick={this.handleArticleSearch}>Search</Button>
                </Form>
                <Toast onClose={this.handleToastShow} show={this.state.searchError} delay={3000} autohide>
                    <Toast.Body>{"No articles matching the keyword "+ this.state.searchTerm + " were found"}</Toast.Body>
                </Toast>
                <Accordion>
                    {this.state.articles.length == 0 ? null :
                        this.state.articles.map((article, index) => (
                            <ArticleComponent article={article} key={index}/>
                        ))
                    }
                </Accordion>
                <Pagination>
                    <Pagination.First href={"/v1/articles?page=1"} onClick={this.handlePageLoad}/>
                    {this.state.previousPage == ""? <Pagination.Prev/>: 
                        <Pagination.Prev href={"/v1/articles?page=" + this.state.previousPage} onClick={this.handlePageLoad}/>}
                    {Array.from(Array(this.state.totalPages), (x, index) => 1 + index * 1).map((pageNum, index) => (
                        <Pagination.Item
                            href={"/v1/articles?page=" + pageNum}
                            key={pageNum}
                            active={pageNum == this.state.currentPage? true : false}
                            onClick={this.handlePageLoad}
                        >{pageNum}
                        </Pagination.Item>
                    ))}
                    {this.state.nextPage == ""? <Pagination.Next/>: 
                        <Pagination.Next href={"/v1/articles?page=" + this.state.nextPage} onClick={this.handlePageLoad}/>}
                    <Pagination.Last href={"/v1/articles?page=" + this.state.totalPages} onClick={this.handlePageLoad}/>
</Pagination>
            </div>
        
        )
    }
}


export default ArticleHomeComponent;