package models
import play.api.libs.json._


case class Page(jsonResult: JsValue)

object Page {
  /**
    * Mapping to serialize an instance of Page to JSON
    */
  implicit val implicitWrites = new Writes[Page] {
    def writes(page: Page): JsValue = {
      val currentPage = (page.jsonResult \ "page_number").as[Int]
      val totalPages = (page.jsonResult \ "total_pages").as[Int]

      Json.obj(
        "count" -> (page.jsonResult \ "total_entries").as[Int],
        "currentPage" -> currentPage,
        "articles" -> (page.jsonResult \ "articles").get,
        "nextPage" -> (if (currentPage < totalPages) currentPage + 1 else ""),
        "previousPage" -> (if (currentPage > 1) currentPage - 1 else ""),
        "pageSize" -> (page.jsonResult \ "page_size").as[Int],
        "totalPages" -> (page.jsonResult \ "total_pages").as[Int]
      )
    }
  }
}


class ArticleId private (val underlying: Int) extends AnyVal {
  override def toString: String = underlying.toString
}

object ArticleId {
  def apply(raw: String): ArticleId = {
    require(raw != null)
    new ArticleId(Integer.parseInt(raw))
  }
}


