package controllers

import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Try,Success, Failure}

import play.api.MarkerContext
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.ws.{WSClient}
import play.api.Configuration

import models._
import services.ArticleService

@Singleton
class ArticleController @Inject()(cc: ControllerComponents, ws: WSClient, config: Configuration)
  extends AbstractController(cc) {
    private val articleService = new ArticleService(ws, config)

    def list(pageNum: Option[String], status: Option[String]) = Action.async {
        val articlesFutureTry = Try {articleService.getArticles(pageNum, status)}
        articlesFutureTry match {
            case Success(articlesFuture) =>
                articlesFuture.map {articlesOption =>
                    articlesOption.map { results =>
                        val page = Page(results)
                        Ok(Json.toJson(page))
                    } getOrElse NotFound(Json.obj("error" -> s"No articles found"))
                }
            case Failure(exception) =>
                exception.getMessage() match {
                    case msg if msg contains "Invalid URL" =>
                        Future(Status(400)
                            (Json.obj("error" ->
                              """
                                |Base URL for Elevio API missing. See how to set it up here:
                                |https://github.com/alexkiura/article_viewer#get-api-key-and-token-from-elevio
                              """.stripMargin
                            )))
                    case _ => Future(Status(400)(Json.obj("error" -> exception.getMessage())))
                }
        }
    }

    def show(articleId: String) = Action.async {implicit request =>
        val articleFutureTry = Try {articleService.getArticle(ArticleId(articleId))}
        articleFutureTry match {
            case Success(articleFuture) =>
                articleFuture.map {articleOption =>
                    articleOption.map { result =>
                        val (statusCode, resultJson) = result
                        Status(statusCode)(resultJson)
                    } getOrElse NotFound(Json.obj("error" -> s"Article matching id $articleId was not found"))
                }
            case Failure(exception) =>
                Future(Status(400)(Json.obj("error" -> exception.getMessage())))
        }
    }

    def search(searchTerm: String) = Action.async {implicit  request =>
        val searchResultsFuture = articleService.searchArticle(searchTerm)
        searchResultsFuture.map {resultsOption =>
          resultsOption.map { result =>
            Ok(result)
          } getOrElse NotFound(Json.obj("error" -> "No articles matching the keyword were found"))
        }
    }
}