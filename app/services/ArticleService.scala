package services


import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Try,Success,Failure}

import play.api.libs.ws.{WSClient, WSRequest}
import play.api.Configuration
import play.api.libs.json._

import models._

class ArticleService(wsClient: WSClient, config: Configuration) {
  private val apiUrl = config.get[String]("elevio.apiUrl")
  private val apiKey = config.get[String]("elevio.apiKey")
  private val authToken = config.get[String]("elevio.authToken")
  private val pageSize = config.get[String]("elevio.pageSize")

  def getArticles(page: Option[String], status: Option[String]): Future[Option[JsValue]] = {

    val request: WSRequest = wsClient.url(s"$apiUrl/articles")

    val secureRequest: WSRequest = request
      .addHttpHeaders("x-api-key" -> apiKey, "Authorization" -> authToken)
      .addQueryStringParameters(
        "page_size" -> pageSize, "page" -> page.getOrElse("1"), "status" -> status.getOrElse("published"))
    val resultsFuture = secureRequest.get()
    resultsFuture.map{ articlesResponse =>
      val resultsJson = articlesResponse.json
      val statusCode = articlesResponse.status
      statusCode match {
        case 404 => None
        case _ => Some(resultsJson)
      }
    }
  }

  def getArticle(id: ArticleId): Future[Option[(Int, JsValue)]] = {
    val request: WSRequest = wsClient.url(s"$apiUrl/articles/$id")
    val secureRequest: WSRequest = request
      .addHttpHeaders("x-api-key" -> apiKey, "Authorization" -> authToken)
    val resultsTry = Try {secureRequest.get() }
    resultsTry match {
      case Success(resultsFuture) => resultsFuture.map{ articleResponse =>
        val statusCode = articleResponse.status
        val articleJson = articleResponse.json
        statusCode match {
          case 200 => Some((statusCode, (articleJson \ "article").get))
          case _ => Some((statusCode, articleJson))
        }
      }
      case Failure(s) => Future {Some(500, (Json.obj("error" -> s"$s"))) }
    }

  }

  def searchArticle(searchTerm: String): Future[Option[JsValue]] = {
    val request: WSRequest = wsClient.url(s"$apiUrl/search/en?query=$searchTerm")
    val secureRequest: WSRequest = request
      .addHttpHeaders("x-api-key" -> apiKey, "Authorization" -> authToken)
    val resultsFuture = secureRequest.get()
    resultsFuture.map{ results =>
      val resultsJson = results.json
      val totalResults = (resultsJson \ "totalResults").as[Int]
      totalResults match {
        case 0 => None
        case _ => Some(resultsJson)
      }
    }
  }

}

