package v1.article

import javax.inject.Inject

import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

import controllers.ArticleController

import models.ArticleId

class ArticleRouter @Inject()(controller: ArticleController) extends SimpleRouter {
    val prefix = "/v1"

    def link(id: ArticleId): String = {
        import io.lemonlabs.uri.dsl._
        val url = prefix / id.toString
        url.toString()
    }
    
    override def routes: Routes = {
        case GET(p"/articles"? q_o"page=$pageNum" & q_o"status=$status") =>
            controller.list(pageNum, status)

        case GET(p"/articles/$id") =>
            controller.show(id)

        case GET(p"/search"? q"query=$searchTerm") =>
            controller.search(searchTerm)
  }
}