# Article Viewer

A program to retrieve and list articles

## What it is

A program that allows a user to:

* Retrieve articles
* Search for articles by keyword
* Access an article's detail

## Prerequisites

* scala: See setup instructions [here](https://www.scala-lang.org/download/)
* sbt: See setup instructions [here](https://www.scala-sbt.org/release/docs/Setup.html)

## Setup

### Get API Key and token from ELEVIO

You need to have an account on Elevio. Obtain an API key and an API token [here](https://app.elevio-staging.com/apikeys)

The key and token are necessary to authenticte requests made to the Elevio REST API.

Grab the keys and set them as environment variables as follows:

```bash
export ELEVIO_URL="https://api.elevio-staging.com/v1"
export API_KEY={YOUR API KEY}
export AUTHORIZATION_TOKEN = {YOUR API TOKEN}
```

### Clone this repo

```
git clone git@github.com:alexkiura/article_viewer.git
```

Navigate to the root folder
```
cd article_viewer
```




## Running the Project

Start the API by running the command below:
```
sbt run 9000
```

## Testing

To run the tests for the app, run
```
sbt test
```

## Endpoints

The API exposes four endpoints:

### /v1/articles

Retrieve a paginated list of articles from the user's account

Allowed methods: *GET*  
Optional query string parameters:

* `page`: Current page

* `status`: `draft` to get drafts and `published` to get only published articles

Usage: `curl -X GET 'https://localhost:9000/v1/articles?page={page}&&status={status}'`

Sample response:

```json
{
    "count": 14,
    "currentPage": 1,
    "articles": [...],
    "nextPage": 2,
    "previousPage": "",
    "pageSize": 10
}
```

### /v1/articles/{id}

Retrieve a specific article identified by the specified id
Allowed methods: *GET*  

Usage: `curl -X GET 'http://localhost:9000/v1/articles/{id}`

```json
{
    "updated_at": "2019-07-06T22:41:19Z",
    "translations": [...],
    "title": "Article 5",
    "status": "published",
    "source": "custom",
    "smart_groups": [],
    "revision": null,
    "order": 999,
    "notes": null,
    "last_publisher": {...},
    "last_published_at": "2019-07-06T22:41:19Z",
    "keywords": [],
    "id": 5,
    "external_id": null,
    "editor_version": "3",
    "created_at": "2019-07-06T22:40:58Z",
    "contributors": [...],
    "category_id": 1,
    "author": {...},
    "access_groups": [],
    "access_emails": [],
    "access_domains": [],
    "access": "public"
}
```

### /v1/search?query={searchTerm}

Search for articles that match a given keyword
Allowed methods: *GET*
Mandatory query string parameters:

* `query`: The search term

Usage: `curl -X GET 'http://localhost:9000/v1/search?query={keyword}'`

Sample response:

```json
{
    "queryTerm": "ddt",
    "totalResults": 12,
    "totalPages": 2,
    "currentPage": 1,
    "count": 8,
    "results": [...]
}
```

## Sample requests

Before making requests, make sure the server is running by running `sbt run`.

## Built with
[Scala](https://www.scala-lang.org/) |
[Play](https://www.playframework.com/) |