name := """article_viewer"""
version := "1.0-SNAPSHOT"
scalaVersion := "2.12.8"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
pipelineStages := Seq(digest)

libraryDependencies ++= Seq(
  caffeine,
  guice,
  ws,
  "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided",
  "io.lemonlabs" %% "scala-uri" % "1.4.10",
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.0" % "test",
  "org.mockito" % "mockito-core" % "2.7.22" % "test"
)

herokuAppName in Compile := "article-vwr"

resolvers += "sonatype-snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
