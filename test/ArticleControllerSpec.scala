import scala.concurrent.Future
import org.scalatestplus.play.PlaySpec
import play.api.test.FakeRequest
import play.api.test.Helpers
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import play.api.libs.json.Json
import play.api.Configuration
import controllers._
import org.scalatest.concurrent.ScalaFutures
import play.api.test.Helpers._

class ArticleControllerSpec extends PlaySpec
  with MockitoSugar
  with ScalaFutures {
  "Article Controller" should {
    "return a list of articles in a paginated list" in {
      val url = "https://api.elevio-staging.com/v1"
      val articlesResponse =
        """{
          |  "total_pages": 1,
          |  "total_entries": 1,
          |  "page_size": 1,
          |  "page_number": 1,
          |  "articles": [
          |    {
          |      "updated_at": "2019-07-05T15:20:16Z",
          |      "translations": [
          |        {
          |          "updated_at": "2019-06-26T01:30:10Z",
          |          "title": "Introducing our in-app help",
          |          "language_id": "en",
          |          "id": 4
          |        }
          |      ],
          |      "title": "Introducing our in-app help",
          |      "subcategory_groups": [],
          |      "status": "published",
          |      "source": "custom",
          |      "smart_groups": [],
          |      "revision_status": "wip",
          |      "order": 1,
          |      "notes": "This is just a sample article to get you going, feel free to edit or remove this article",
          |      "keywords": [],
          |      "id": 1,
          |      "has_revision": true,
          |      "external_id": null,
          |      "editor_version": "2",
          |      "category_id": 1,
          |      "category_groups": [],
          |      "article_groups": [],
          |      "access_emails": [],
          |      "access_domains": [],
          |      "access": "public"
          |    }
          |  ]
          |}
        """.stripMargin
      val articlesJson= Json.parse(articlesResponse)
      val wsClientStub = mock[WSClient]
      val wsRequestStub = mock[WSRequest]
      val wsResponseStub = mock[WSResponse]
      val configurationStub = mock[Configuration]
      when(configurationStub.get[String]("elevio.apiUrl")).thenReturn(url)
      when(configurationStub.get[String]("elevio.apiKey")).thenReturn("apiKey")
      when(configurationStub.get[String]("elevio.authToken")).thenReturn("authToken")
      when(configurationStub.get[String]("elevio.pageSize")).thenReturn("10")
      when(wsResponseStub.json).thenReturn(articlesJson)
      when(wsRequestStub.get()).thenReturn(Future.successful(wsResponseStub))
      when(wsClientStub.url(s"$url/articles")).thenReturn(wsRequestStub)
      when(wsRequestStub.addHttpHeaders(
        "x-api-key" -> "apiKey", "Authorization" -> "authToken")).thenReturn(wsRequestStub)
      when(wsRequestStub.addQueryStringParameters(
        "page_size" -> "10", "page" -> "1", "status" -> "published")).thenReturn(wsRequestStub)
      when(wsClientStub.url(url)).thenReturn(wsRequestStub)

      val controller = new ArticleController(Helpers.stubControllerComponents(), wsClientStub, configurationStub)
      val results = controller.list(Option[String]("1"), Option[String]("published")).apply(FakeRequest())
      val resultsJson = contentAsJson(results)
      val currentPage = (resultsJson \ "currentPage").as[Int]
      val pageSize = (resultsJson \ "pageSize").as[Int]
      currentPage mustEqual 1
      pageSize mustEqual 1
      }
    }
  }


